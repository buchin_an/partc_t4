package com.company;

import java.util.Scanner;

public class Main {

    private static final int COUNT = 3;
    private static final int LENGTH = 3;

    public static void main(String[] args) {

        String[] strings = new String[COUNT];
        Scanner scanner = new Scanner(System.in);

        for (int i = 0; i < strings.length; i++) {
            strings[i] = scanner.nextLine();
        }
        for (String string : strings) {
            String[] tempSTR = string.split(" ");
            StringBuilder stringBuilder = new StringBuilder();
            for (String s : tempSTR) {
                if (isVowel(s)) {
                    stringBuilder.append(s);
                    stringBuilder.append(" ");
                }
            }
            System.out.println(stringBuilder.toString()
                    .substring(0, stringBuilder.toString().length() - 1));
        }
    }

    private static boolean isVowel(String s) {
        if (s.length()!=LENGTH){
            return true;
        }
        char[] vowels = {'а', 'я', 'у', 'ю', 'и', 'ы', 'э', 'е', 'о', 'ё'};

        for (char vowel : vowels) {
            if (s.toLowerCase().charAt(0) == vowel) {
                return true;
            }
        }
        return false;
    }
}
